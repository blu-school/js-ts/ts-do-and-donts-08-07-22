interface UserAPI {
  data: {
    name: string;
    age: number;
    role: string;
    relation: string;
    company: string;
  };
  pagination: {
    page: number;
    count: number;
    nextPage: number;
  };
}

interface UserInfo {
  name: string;
  age: number;
  role: string;
}

const getCurrentUserInfo = (): UserInfo => {
  const userResponse: UserAPI = {
    data: {
      name: "Simeon",
      age: 20,
      role: "developer",
      relation: "fe",
      company: "blubito",
    },
    pagination: {
      page: 1,
      count: 1,
      nextPage: 0,
    },
  };

  const userInfo = {
    name: userResponse.data.name,
    age: userResponse.data.age,
    role: userResponse.data.role
  };

  return userInfo;
};

const myUser = getCurrentUserInfo();