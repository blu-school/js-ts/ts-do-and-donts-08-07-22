/**
 * Usage of optional params
 */

/**
 * ----------------------
 * Example - probably not the best
 */

interface Billing {
  id: string;
  name: string;
  postal: string;
  city: string;
  address: string;
  countryId: string;
  vat: string;
  company: string;
  email: string;
  title: string | null;
  salutation: string | null;
}

interface Owner {
  id: string;
  name: string;
  email: string;
  title: string | null;
  salutation: string | null;
}

export interface Tenant {
  id: string;
  company: string;
  postal: string;
  city: string;
  address: string;
  countryId: string;
  licenseId: string | null;
  vat: string;
  language: string;
  phoneNumber: string;
  locked: boolean;
  salesChannel: string;
}

const tenant: TenantWithBilling = {
  id: "some-str",
  company: "some-str",
  postal: "some-str",
  city: "some-str",
  address: "some-str",
  countryId: "some-str",
  licenseId: "some-str",
  vat: "some-str",
  language: "some-str",
  phoneNumber: "some-str",
  locked: false,
  salesChannel: "some-str",
  billingAddress: {
    id: "some-str",
    company: "some-str",
    name: "some-str",
    postal: "some-str",
    city: "some-str",
    address: "some-str",
    countryId: "some-str",
    vat: "some-str",
    email: "some-str",
    title: "some-str",
    salutation: "some-str",
  },
};

const tenantData = tenant.billingAddress.id;

export interface TenantWithBilling extends Tenant {
  billingAddress: Billing;
  access?: string;
}

export interface TenantWithBillingAndOwner extends TenantWithBilling {
  owner: Owner;
}

/**
 * ----------------------
 * Example - it's ok
 */
interface NotificationParams {
  title: string;
  message: string;
  link: string;
  linkMessage: string;
  icon: string;
  duration?: number;
}

enum NotifcationTypes {
  error = "error",
  success = "success",
  warning = "warning",
  info = "info",
}

const ElNotification = (props: any) => props;

const buildNotification = (
  params: NotificationParams,
  type: NotifcationTypes
) => {
  ElNotification({
    title: params.title,
    message: params.link
      ? `<p class='mb-4'>${params.message}</p><i class='${params.icon} pr-2'></i> <a class='inline-block underline font-size-base-m' href='${params.link}'>${params.linkMessage}</a>`
      : params.message,
    duration: params.duration,
    type,
    position: "bottom-right",
    dangerouslyUseHTMLString: !!params.link,
    customClass: `notification ${type}`,
    offset: 100,
  });
};

export function getSuccessNotification({
  duration = 6000,
  ...params
}: NotificationParams) {
  buildNotification({ ...params, duration }, NotifcationTypes.success);
}

export function getErrorNotification(params: NotificationParams) {
  if (params.duration === undefined) params.duration = 0;
  buildNotification(params, NotifcationTypes.error);
}
