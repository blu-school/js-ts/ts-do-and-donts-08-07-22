import { TenantWithBilling } from "./04-optional";

interface User {
  firstName: string;
  lastName: string;
  role: string;
  username: string;
  email: string;
  isAdmin: boolean;
}

const adminUser: User = {
  firstName: "Some",
  lastName: "Name",
  role: "Admin",
  username: "super.admin",
  email: "super.admin@gmail.com",
  isAdmin: true,
};

const getProperty = <T, K extends keyof T>(data: T, field: K): T[K] => {
  return data[field];
};

const username = getProperty(adminUser, "role");

const tenant: TenantWithBilling = {
  id: "some-str",
  company: "some-str",
  postal: "some-str",
  city: "some-str",
  address: "some-str",
  countryId: "some-str",
  licenseId: "some-str",
  vat: "some-str",
  language: "some-str",
  phoneNumber: "some-str",
  locked: false,
  salesChannel: "some-str",
  billingAddress: {
    id: "some-str",
    company: "some-str",
    name: "some-str",
    postal: "some-str",
    city: "some-str",
    address: "some-str",
    countryId: "some-str",
    vat: "some-str",
    email: "some-str",
    title: "some-str",
    salutation: "some-str",
  },
};
// const field = "id";
const tenantFiled = getProperty(tenant, "access");
