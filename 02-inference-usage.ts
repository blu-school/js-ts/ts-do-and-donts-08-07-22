import { Tenant } from "./04-optional";
import { ParamsFetchPages, fetchAllTenantsInPages } from "./02-inference";
// In your component/file
class SomeComponent {
  tenant: Tenant[] = [];
  pagination: object | undefined = undefined;

  fetchTenants = async () => {
    const fetchTenantsVariable: ParamsFetchPages = {
      pagination: { page: 1 },
      sorting: "",
      filters: "name:simeon",
    };
    // Here we don't need any types
    const res = await fetchAllTenantsInPages(fetchTenantsVariable);
    this.tenant = res.data;
    this.pagination = res.pagination;
  };
}
