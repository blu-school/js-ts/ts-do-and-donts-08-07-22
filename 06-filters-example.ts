interface Row {
  id: string;
  property: string;
  label: string;
  subrow: Subrow[];
}

interface Subrow extends Omit<Row, 'subrow'>{};

const row: Row = {
  id: "1",
  label: "label",
  property: "prop",
  subrow: [
    {
      id: "1.1",
      label: "label_1",
      property: "prop_1"
    }
  ],
};


