// A data provider
import axios from "axios";
import { Tenant } from "./04-optional";

export interface ParamsFetchPages {
  pagination: object;
  sorting: string;
  filters: any;
}

export interface TenantsGET {
  data: Tenant[];
  pagination: object;
}

export async function fetchAllTenantsInPages(
  fetchParams: ParamsFetchPages
): Promise<TenantsGET> {
  try {
    const res = await axios.get<TenantsGET>("my-api");
    return res.data as any;
  } catch (error) {
    console.error(error);
    return {
      data: [],
      pagination: {},
    };
  }
}
