// We have a lot of stuff here
interface Translation {
  de: string;
  en: string;
}

interface FeatureAssingment {
  id?: string;
  productId: string;
  productName: Translation;
  featureId: string;
  featureName: Translation;
  featureRoleId: string;
  featureRoleName: Translation;
}

interface AvailableProduct {
  name: string;
  id: string;
  priceInCent: number;
  numberOfLicenses: number;
}

interface AvailableRole {
  description?: string;
  id: string;
  name: string;
  permissions?: any;
}

export interface User {
  id: string;
  tenantId: string;
  name: string;
  email: string;
  role: string;
  products: string[];
  title: string;
  salutation: string;
  featureAssignments: FeatureAssingment[];
  blocked?: boolean;
  emailVerified?: boolean;
  lastLogin?: string;
  tenantName?: string;
  availableProducts?: AvailableProduct[];
  availableRoles?: AvailableRole[];
}

// "Duplication"
export interface FeatureRole {
  id: string;
  name: Translation;
  description: Translation;
}

export interface FeatureAssignment {
  id: string;
  productId: string;
  featureId: string;
  featureRoleId: string;
  productName: Translation;
  featureName: Translation;
  featureRoleName: Translation;
}
